﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CredoCommon.Enums
{
    public enum TaskManagerStatus
    {
        Open = 1,
        Completed = 2,
        Close = 3
    }

}
