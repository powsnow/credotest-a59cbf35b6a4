﻿using Credo.Models.Interfaces;
using CredoCommon.Enums;
using CredoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Credo.DAL
{
    public class TestDAL : ICredoDAL
    {
        private static IList<CredoModels.Models.TaskStatus> taskStatusList;
        private static List<CredoTask> taskList;
        public static List<CredoTask> TaskList
        {
            get
            {
                lock (new object())
                {
                    if (taskList == null)
                    {
                        taskList = new List<CredoTask>();
                    }
                }
                return taskList;
            }
        }
        public static IList<CredoModels.Models.TaskStatus> TaskStatusList
        {
            get
            {
                lock (new object())
                {
                    if (taskStatusList == null)
                    {
                        taskStatusList = new List<CredoModels.Models.TaskStatus>();
                        taskStatusList.Add(new CredoModels.Models.TaskStatus() { ID = (int)TaskManagerStatus.Open, StatusName = TaskManagerStatus.Open.ToString() });
                        taskStatusList.Add(new CredoModels.Models.TaskStatus() { ID = (int)TaskManagerStatus.Completed, StatusName = TaskManagerStatus.Completed.ToString() });
                        taskStatusList.Add(new CredoModels.Models.TaskStatus() { ID = (int)TaskManagerStatus.Close, StatusName = TaskManagerStatus.Close.ToString() });
                    }
                }
                return taskStatusList;
            }
        }
        public async Task<IList<CredoTask>> GetTaskList()
        {
            return await Task.FromResult<IList<CredoTask>>(TaskList);
        }

        public async Task AddTask(CredoModels.CredoTask task)
        {
            int newID = 1;
            if (TaskList.Count > 0)
                newID = TaskList.Last().ID + 1;
            task.ID = newID;
            task.IsCompleted = false;
            await Task.Run(() => { TaskList.Add(task); });
        }

        public async Task DeleteTask(int id)
        {
            await Task.Run(() =>
            {
                if (TaskList.Count > 0)
                    TaskList.Remove(TaskList.Single(t => t.ID == id));
            });
        }

        public async Task UpdateTask(CredoModels.CredoTask task)
        {
            await Task.Run(() =>
            {
                var taskToUpdate = TaskList.Single(t => t.ID == task.ID);
                taskToUpdate.IsCompleted = task.IsCompleted;
            });
        }        
    }
}
