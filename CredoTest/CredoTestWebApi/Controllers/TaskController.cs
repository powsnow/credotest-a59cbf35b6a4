﻿using Credo.Logger;
using Credo.Models.Interfaces;
using CredoBLL;
using CredoModels;
using CredoWebApi.DependencyResolution;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CredoWebApi.Controllers
{
    public class TaskController : ApiController
    {
        private ITaskBLL _taskBLL;
        private ILog _log;
        public TaskController(ITaskBLL taskBLL, ILog log)
        {
            this._taskBLL = taskBLL;
            this._log = log;
        }
        // GET api/task
        /// <summary>
        /// Gets all Tasks from the server.
        /// </summary>
        [Route("api/tasks")]
        [HttpGet]
        public async Task<IHttpActionResult> GetTasks()
        {
            try
            {                
                return Ok(await _taskBLL.GetTaskList());
            }
            catch (Exception ex)
            {
                _log.Write(ex);
                return InternalServerError();
            }
        }

        // POST api/task
        // <summary>
        /// Updates Task by CredoTask model.
        /// </summary>
        [Route("api/task/update")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateTask([FromBody]CredoTask task)
        {
            try
            {
                await _taskBLL.UpdateTask(task);
                return Ok();
            }
            catch (Exception ex)
            {
                _log.Write(ex);
                return InternalServerError();
            }
        }

        // PUT api/task/5
        [Route("api/task/add")]
        [HttpPost]
        // <summary>
        /// Adds a new task
        /// </summary>
        public async Task<IHttpActionResult> AddTask([FromBody]CredoTask task)
        {
            try
            {
                await _taskBLL.AddTask(task);
                return Ok();
            }
            catch (Exception ex)
            {
                _log.Write(ex);
                return InternalServerError();
            }
        }

        // DELETE api/task/5
        [Route("api/task/delete")]

        public async Task<IHttpActionResult> DeleteTask(int id)
        {
            try
            {
                await _taskBLL.DeleteTask(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _log.Write(ex);
                return InternalServerError();
            }
        }
    }
}
