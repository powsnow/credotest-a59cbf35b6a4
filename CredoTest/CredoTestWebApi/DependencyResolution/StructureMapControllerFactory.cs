﻿using System;
using System.Web.Mvc;
using StructureMap;


namespace CredoWebApi.DependencyResolution
{
    public class StructureMapControllerFactory : DefaultControllerFactory
    {
        protected  IController GetControllerInstance(Type controllerType)
        {
            return (IController)ObjectFactory.GetInstance(controllerType);
        }
    }
}