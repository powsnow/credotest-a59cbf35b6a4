﻿using Credo.DAL;
using Credo.Models.Interfaces;
using CredoCommon.Enums;
using CredoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CredoBLL
{
    public class TaskBLL : ITaskBLL
    {        
        private ICredoDAL _dal;
        public TaskBLL()
        {
            this._dal = new TestDAL();
        }
        public TaskBLL(ICredoDAL dal)
        {
            this._dal = dal;
        } 
        public async Task<IList<CredoTask>> GetTaskList()
        {

            return await _dal.GetTaskList();
        }
        public async Task AddTask(CredoTask task)
        {
            await _dal.AddTask(task);
        }
        public async Task DeleteTask(int id)
        {
            await _dal.DeleteTask(id);
        }
        public async Task UpdateTask(CredoTask task)
        {
            await _dal.UpdateTask(task);
        }
    }

}
