﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Credo.UI.Startup))]
namespace Credo.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
