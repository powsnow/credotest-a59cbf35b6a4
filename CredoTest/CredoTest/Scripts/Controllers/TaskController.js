﻿TaskApp.controller('TaskController', function ($scope, TaskService) {
    $scope.currentPage = 0;
    $scope.pageSize = 2;
    getAllTasks();

    function getAllTasks() {
        TaskService.getAllTasks()
            .success(function (tasks) {
                $scope.tasks = tasks;
                $scope.numberOfPages = function () {
                    return Math.ceil($scope.tasks.length / $scope.pageSize);
                }
                console.log($scope.tasks);
            })
            .error(function (error) {
                $scope.status = 'Unable to load task data: ' + error.message;
                console.log($scope.status);
            });
    }    
});
//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
TaskApp.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});