﻿$(document).ready(function () {
    GetAllTasks();
    $("#newTask").hide();
});
function GetAllTasks() {
    jQuery.support.cors = true;
    $.ajax({
        url: 'http://localhost/CredoWebApi/api/tasks',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            WriteResponse(data);
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}
function WriteResponse(tasks) {
    var strResult = "<table><th>Task ID</th><th>Task Description</th><th>Is Completed</th>";
    var isChecked = "";
    $.each(tasks, function (index, task) {
        if (task.IsCompleted) {
            isChecked = "checked"
        }
        else {
            isChecked = "";
        }
        strResult += "<tr><td>" + task.ID + "</td><td> " + task.TaskDescription + "</td><td><input type='checkbox' id ='chkIsCompleted_" + task.ID + "' " + isChecked + " ></input></td><td><button onclick=\"UpdateTask(" + task.ID + ",'chkIsCompleted_" + task.ID + "');return false;\">Update</button></td></tr>";
    });
    strResult += "</table>";
    $("#divResult").html(strResult);
}
function AddTask() {
    $("#newTask").show();
    $("#btnAddTask").hide();
}
function SaveTask() {
    $("#newTask").hide();
    AddNewTask();
    $("#txtTaskDesc").val('');
    $("#btnAddTask").show();
}
function AddNewTask() {
    jQuery.support.cors = true;
    var task = {
        ID: 0,
        TaskDescription: $('#txtTaskDesc').val()
    };

    $.ajax({
        url: 'http://localhost/CredoWebApi/api/task/add',
        type: 'POST',
        data: JSON.stringify(task),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            GetAllTasks();
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}
function UpdateTask(taskID, chkIsCompletedID) {
    var task = {
        ID: taskID,
        IsCompleted: $('#' + chkIsCompletedID).is(':checked')
    };
    $.ajax({
        url: 'http://localhost/CredoWebApi/api/task/update',
        type: 'POST',
        data: JSON.stringify(task),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            GetAllTasks();
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}