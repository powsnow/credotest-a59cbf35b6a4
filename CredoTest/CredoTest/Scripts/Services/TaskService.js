﻿TaskApp.factory('TaskService', ['$http', function ($http) {

    var TaskService = {};
    TaskService.getAllTasks = function () {
        return $http.get('http://localhost/CredoWebApi/api/tasks');
    };

    TaskService.addNewTask = function (task) {
        return $http({
            method: "POST",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: 'http://localhost/CredoWebApi/api/task/add',
            data: $.param({ CredoTask: task })
        });
    };

    TaskService.updateTask = function (task) {
        return $http({
            method: "POST",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: 'http://localhost/CredoWebApi/api/task/update',
            data: $.param({ CredoTask: task })
        });
    };
    return TaskService;

}]);