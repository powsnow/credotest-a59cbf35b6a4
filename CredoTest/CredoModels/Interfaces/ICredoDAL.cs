﻿using CredoModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Credo.Models.Interfaces
{
    public interface ICredoDAL
    {
        Task<IList<CredoTask>> GetTaskList();
        Task AddTask(CredoTask task);
        Task DeleteTask(int id);
        Task UpdateTask(CredoTask task);        
    }
}
