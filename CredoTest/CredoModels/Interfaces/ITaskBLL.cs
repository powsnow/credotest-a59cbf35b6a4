﻿using CredoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credo.Models.Interfaces
{
    public interface ITaskBLL
    {
        Task<IList<CredoTask>> GetTaskList();
        Task AddTask(CredoTask task);
        Task DeleteTask(int id);
        Task UpdateTask(CredoTask task);        
    }
}
