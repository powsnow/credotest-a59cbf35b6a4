﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credo.Models.Interfaces
{
    public interface ILog
    {
        void Write(Exception ex);
    }
}
