﻿using CredoCommon.Enums;
using CredoModels.Models;

namespace CredoModels
{
    public class CredoTask
    {
        public int ID { set; get; }
        public string TaskDescription { set; get; }
        public bool IsCompleted { set; get; }
    }
}
