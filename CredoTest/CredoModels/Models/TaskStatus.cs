﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CredoModels.Models
{
    public class TaskStatus
    {
        public int ID { set; get; }
        public string StatusName { set; get; }
    }
}
